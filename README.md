Vagrant will drive VirtualBox

https://www.vagrantup.com/

https://www.virtualbox.org/

For this specific case, you can launch either a single node or a cluster.

You may want to increase the CPU & memory to make it usable.


```
# Start a single node from shell.
vagrant up single

# When the machine is up you can SSH into it using
vagrant ssh single

# You should be able to access RStudio Server via
http://localhost:8787

# When you're ready to shut the machine down you can either do a shutdown:
vagrant halt single

# or a suspend
vagrant suspend single

# If you halted, you can restart using
vagrant up single

# If you suspended, you can restart using
vagrant resume single
```